package com.diplo.infraestructure.msreserva.entityframework.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import com.diplo.msreserva.model.reserva.Reserva;
import com.diplo.msreserva.model.vuelo.Vuelo;

@Entity
@Table(name = "vuelo")
public class VueloEntity {
	@Id
	private String VueloId;
	private int NroVuelo;
	private int CantidadAsientoDisponible;
	private String Destino;
	

	public VueloEntity() {
	}


	public VueloEntity(Vuelo aux) {
		super();
		VueloId = aux.getId().toString();
		NroVuelo = aux.getNroVuelo().getNumero();
		CantidadAsientoDisponible = aux.getCantidadAsientoDisponible().getDisponibilidad();
		Destino = aux.getDestino().getNombreDestino();
	}


	public String getVueloId() {
		return VueloId;
	}


	public void setVueloId(String vueloId) {
		VueloId = vueloId;
	}


	public int getNroVuelo() {
		return NroVuelo;
	}


	public void setNroVuelo(int nroVuelo) {
		NroVuelo = nroVuelo;
	}


	public int getCantidadAsientoDisponible() {
		return CantidadAsientoDisponible;
	}


	public void setCantidadAsientoDisponible(int cantidadAsientoDisponible) {
		CantidadAsientoDisponible = cantidadAsientoDisponible;
	}


	public String getDestino() {
		return Destino;
	}


	public void setDestino(String destino) {
		Destino = destino;
	}

}
